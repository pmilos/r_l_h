#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool test = false;

bool droga(int mapa[4][4],int sciezka[4][4], int x, int y, int wymiar)
{
	if (test == true)
	{
		return false;
	}
	else if (x >= wymiar || y >= wymiar)
	{
		return false;
	}
	else if (x < 0 || y < 0)
	{
		return false;
	}
	else if ((mapa[x][y] == 1) || (sciezka[x][y] == 1))
	{
		return false;
	}
	else if (x == 0 && y == 0)
	{
		test = true;
		return false;
	}
	else if (mapa[x-1][y] == false && sciezka[x-1][y] == false)
	{
		sciezka[x][y] = true;
		droga(mapa, sciezka, x - 1, y, 4);
	}
	else if (mapa[x][y-1] == false && sciezka[x][y-1] == false)
	{
		sciezka[x][y] = true;
		droga(mapa, sciezka, x, y - 1, 4);
		
	}
	else if (mapa[x+1][y] == false && sciezka[x+1][y] == false)
	{
		sciezka[x][y] = true;
		droga(mapa, sciezka, x + 1, y, 4);

	}
	else if (mapa[x][y+1] == false && sciezka[x][y+1] == false)
	{
		sciezka[x][y] = true;
		droga(mapa, sciezka, x, y + 1, 4);
	
	}
	return false;
}

int main()
{
	
	int mapa[4][4] = 
	{
		{ 0, 0, 0, 0 },
		{ 1, 0, 0, 0 },
		{ 0, 0, 1, 1 },
		{ 0, 0, 0, 0 }
	};
	
	

	int sciezka[4][4] =
	{
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 }
	};

	droga(mapa, sciezka, 3, 3, 4);

	for (size_t i = 0; i < 4; i++)
	{
		for (size_t j = 0; j < 4; j++)
		{
			printf("%i ", sciezka[i][j]);
		}
		puts("");
	}













	system("PAUSE");
	return EXIT_SUCCESS;
}